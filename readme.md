 # -- Instruction

## Installation global du projet

1. Récupérer le repository du projet : \
   `git clone git@gitlab.com:ValentinArg/ru_direct.git` \
   `cd ru_direct` \
   `git remote rename origin old-origin` \
   `git remote add origin git@gitlab.com:ValentinArg/ru_direct.git` \

2. lancer les conteneurs : \
   `docker-compose up -d --build`

3. Vérification de la disponibilité des containers en cour d'exécution : \
   docker-compose ps

4. Installation et mise à jours des dépendances de laravel : \
   `docker run --rm -v $pwd/ru_site:/app composer install` \
   NB: `$pwd` permet de récuperer le chemin absolu du dossier courant. Cette commande peut varier selon votre système. \
   Windows PowerShell : \$pwd \
   Linux : `pwd`

5. Les répertoire dans storage doivent être accessible en écriture par note serveur web
   `sudo chmod -R +w storage/`
6. Tester avec un navigateur: http://localhost:80

## Utiliser Laravel Artisan

1. Lancer les commandes artisan depuis notre image app avec la commande php artisan: \
   `docker-compose exec app php artisan key:generate`
2. Lancer la commande artisan pour migrer les demandés dans la base de données: \
   `docker-compose exec app php artisan migrate`

## Utiliser VueJS

VueJS, contrairement aux fichiers php et blade, doit être compilé à part lors du développement de front-end.

1. Se placer dans le bon dossier: \
   `cd ru_site`
2. Installer les dépendances npm: \
   `npm i`
3. Compiler en mode développement (chaque changement ne nécessitera qu'un reload): \
   `npm run watch`

## Connexion au serveur de base de donnée phpmyadmin \

serveur=mariadb \
DB_DATABASE=grevistes \
DB_USERNAME=root \
DB_PASSWORD=lesgrevistes

## Connexion au premier compte (administrateur) \

Email = nuitinfo.grevistes@gmail.com
MDP = InfoGrevistes2020

## Compte mail support (administrateur) \

Email = supprt.grevistes@gmail.com
MDP = SupportGrevistes2020
