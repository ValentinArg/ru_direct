FROM php:7.2.5-fpm

# install system dependencies
RUN apt-get update && apt-get install -y mysql-client --no-install-recommends \
 && docker-php-ext-install pdo_mysql && \
     apt-get install -y \
         apt-utils \
         libzip-dev libnss3 \
         chromium \
         curl \
         git \
         xvfb gtk2-engines-pixbuf \
         imagemagick x11-apps \
         zip \
         nano \
         cron \
         unzip \
         && docker-php-ext-install zip && apt-get clean && rm -rf /var/lib/apt/lists/*